<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace MasterApp\ExceptionLogger;

use DateTime;
use Exception;
use JsonException;
use MasterApp\Mattermost\Mattermost;
use MasterApp\Mattermost\MattermostException;
use Nette\Http\IRequest;
use RuntimeException;
use stdClass;

/**
 * Class ExceptionLogger
 * @package MasterApp\ExceptionLogger
 */
class ExceptionLogger {

    private const Line = '----------------------------------------------------------------------------------------------------';

    public string $logPath;

    public string $version;

    // If enabled debug mode [DI]
    public bool $debugModeEnabled;

    private Mattermost $mattermost;

    /** @param Mattermost $mattermost */
    public function __construct(Mattermost $mattermost) {

        $this->mattermost = $mattermost;
    }

    public function setLogPath(string $logPath): void {

        $this->logPath = $logPath;
    }

    public function setVersion(string $version): void {

        $this->version = $version;
    }

    public function setDebugModeEnabled(bool $debugModeEnabled): void {

        $this->debugModeEnabled = $debugModeEnabled;
    }

    /**
     * @param Exception $exception
     * @param IRequest $request
     * @throws ExceptionLoggerException
     */
    public function generateExceptionLog(Exception $exception, IRequest $request): void {

        if ($this->checkIsMattermost($request)) {
            return;
        }
        $logPathWithDir = $this->setUpLogDir('exception');
        $notify = ! $this->debugModeEnabled ? '<!channel>' : '';
        $debugMode = $this->debugModeEnabled ? 'debugMode' : 'prodMode';

        $debug = $this->getRequestDebugObject($request);
        $debug->code = $exception->getCode();
        $debug->exMsg = $exception->getMessage();

        $message = "** :apierror: APP exception thrown **\n";
        $message .= "Please review the error output. {$notify}\n";
        $message .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n\n";
        $message .= "| Type          | Info                         |
                     | :------------ |:----------------------------:|
                     | Environment   | {$debugMode}                 |
                     | ExCode        | {$debug->code}               |
                     | ExMessage     | `{$debug->exMsg}`            |
                     | URL           | {$debug->url}                |
                     | Remote        | {$debug->remote}             |
                     | Body          | {$debug->body}               |
                     | Headers       | `{$debug->headers}`          |\n";

        try {
            $this->mattermost->sendMessage($message);
        } catch (MattermostException $e) {
            throw new ExceptionLoggerException('Can not send mattermost message', $e->getCode(), $e);
        }

        // Log locally
        try {
            $outputData = "Exception log\n";
            $outputData .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n";
            $outputData .= json_encode($debug, JSON_THROW_ON_ERROR);
            $outputData .= "\n\n" . self::Line . "\n";
        } catch (JsonException $jsonException) {
            throw new ExceptionLoggerException('Can not encode local debug data', $jsonException->getCode(), $jsonException);
        }
        $fileName = (new DateTime())->format('Y-m-d') . '-ERR.txt';
        file_put_contents($logPathWithDir . $fileName, $outputData, FILE_APPEND);
    }

    /**
     * @param IRequest $request
     * @param stdClass $metaObject
     * @param string $ip
     * @param bool $useMattermost
     * @throws ExceptionLoggerException
     */
    public function generateScrapeBlockerLog(IRequest $request, stdClass $metaObject, string $ip, bool $useMattermost = false): void {

        if ($this->checkIsMattermost($request)) {
            return;
        }
        $logPathWithDir = $this->setUpLogDir('scrapeProtection');
        $debug = $this->getRequestDebugObject($request);
        $debugMode = $this->debugModeEnabled ? 'debugMode' : 'prodMode';

        try {
            $debugConflicted = json_encode($metaObject, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new ExceptionLoggerException('Can not encode debug information', $e->getCode(), $e);
        }

        $message = "** :apierror: APP Scrape Protection warning thrown **\n";
        $message .= "Please review the error output.\n";
        $message .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n\n";
        $message .= "| Type          | Info                             |
                     | :------------ |:--------------------------------:|
                     | Version/Debug | {$this->version} / {$debugMode}  | 
                     | URL           | {$debug->url}                    |
                     | IP            | {$ip}                            |
                     | Client IP     | {$debug->remote}                 |
                     | COUNT         | {$debugConflicted}               |
                     | Body          | `{$debug->body}`                 |
                     | Headers       | `{$debug->headers}`              |\n";

        if ($useMattermost) {
            try {
                $this->mattermost->sendMessage($message, true);
            } catch (MattermostException $e) {
                throw new ExceptionLoggerException('Can not send mattermost message', $e->getCode(), $e);
            }
        }

        // Log locally
        try {
            $outputData = "Scrape protection log\n";
            $outputData .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n";
            $outputData .= json_encode($debug, JSON_THROW_ON_ERROR);
            $outputData .= "\n\n" . self::Line . "\n";
        } catch (JsonException $jsonException) {
            throw new ExceptionLoggerException('Can not encode local debug data', $jsonException->getCode(), $jsonException);
        }
        $fileName = (new DateTime())->format('Y-m-d') . '-INFO.txt';
        file_put_contents($logPathWithDir . $fileName, $outputData, FILE_APPEND);
    }

    private function setUpLogDir(string $name): string {

        $logPathWithDir = $this->logPath . "/{$name}/";
        if (! file_exists($logPathWithDir) && ! mkdir($logPathWithDir, 0777, true) && ! is_dir($logPathWithDir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $logPathWithDir));
        }
        return $logPathWithDir;
    }

    /**
     * @param IRequest $request
     * @return bool
     */
    private function checkIsMattermost(IRequest $request): bool {

        $agent = $request->getHeader('user-agent');
        return empty($agent) || str_contains($agent, 'mattermost');
    }

    /**
     * @param IRequest $request
     * @return stdClass
     * @throws ExceptionLoggerException
     */
    private function getRequestDebugObject(IRequest $request): stdClass {

        try {
            $debug = new stdClass();
            $debug->url = $request->getUrl();
            $debug->remote = $request->getRemoteAddress();
            $debug->headers = json_encode($request->getHeaders(), JSON_THROW_ON_ERROR);
            $debug->body = json_encode($request->getRawBody(), JSON_THROW_ON_ERROR);
            return $debug;
        } catch (JsonException $exception) {
            throw new ExceptionLoggerException('Can not encode debug request info', $exception->getCode(), $exception);
        }
    }
}