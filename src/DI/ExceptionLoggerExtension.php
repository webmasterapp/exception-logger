<?php

declare(strict_types=1);

namespace MasterApp\ExceptionLogger\DI;

use MasterApp\ExceptionLogger\ExceptionLogger;
use Nette\DI\CompilerExtension;

/**
 * Class ExceptionLoggerExtension
 * @package MasterApp\ExceptionLogger\DI
 */
class ExceptionLoggerExtension extends CompilerExtension {

    public function loadConfiguration(): void {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('exceptionLogger'))
            ->setType(ExceptionLogger::class)
            ->addSetup('setLogPath', [$config['logPath']])
            ->addSetup('setVersion', [$config['version'] ?? 'Undefined'])
            ->addSetup('setDebugModeEnabled', [$config['debugModeEnabled'] ?? true]);
    }
}
