php -d memory_limit=4G ./vendor/bin/phpstan clear-result-cache -c ./phpstan.neon
php -d memory_limit=4G ./vendor/bin/phpstan analyse -c ./phpstan.neon
./vendor/bin/ecs check --clear-cache ./src --fix
./vendor/bin/ecs check --clear-cache ./src
